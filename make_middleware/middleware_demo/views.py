from django.shortcuts import render
from django.template.response import TemplateResponse, HttpResponse


# Create your views here.


def index(request):
    context = {}
    return TemplateResponse(request, 'middleware_demo/index.html', context)


def see_request(request):
    text = f"""
            scheme : {request.scheme}
            path : {request.path}
            method : {request.method}
            GET : {request.GET}
            POST : {request.POST}
            user : {request.user}
            
            """
    return HttpResponse(text, content_type='text/plain')