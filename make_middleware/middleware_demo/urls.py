from django.urls import path
from .views import index, see_request
app_name = 'middleware_demo'
urlpatterns = [
    path('demo/',index, name='index'),
    path('requests/', see_request, name='requests'),
]